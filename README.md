# Services
Servicios VePapá® dockerizados. Gracias a tj que rompió todo.

## Servicios disponibles
### /syncplay
Servidor de syncplay

### /pepemagnetbot
Bot de pelis

### /quake
Servidor de quake

## Ejecución
1. Clonar este repositorio ejecutando `git clone --recurse-submodules git@gitlab.com:vepapa/services.git`
2. Ubicarse en el servicio a ejecutar y luego `docker-compose up -d`